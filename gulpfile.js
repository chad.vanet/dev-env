const gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	sassGlob 	= require('gulp-sass-glob'),
	minifyCSS 	= require('gulp-clean-css'),
	stripDebug 	= require('gulp-strip-debug'),
	sourcemaps 	= require('gulp-sourcemaps'),
	//uglify 		= require('gulp-uglify'),
	concat 		= require('gulp-concat'),
	imagemin 	= require('gulp-imagemin'),
	browserSync = require('browser-sync'),
	del 		= require('del'),
	zip 		= require('gulp-zip'),
	rename 		= require('gulp-rename'),
	folders 	= require('gulp-folders'),
	ifElse 		= require('gulp-if-else'),
	path 		= require('path'),
	watch		= require('gulp-watch'),
	inquirer	= require('inquirer'),
	replace		= require('gulp-replace'),
	fs			= require('fs'),
	run			= require('gulp-run');

/**
* Paths settings
*/
const	libraryPath			= path.normalize("src/library/"),
		piecescontentPath	= path.normalize("src/content/"),
		distPath			= path.normalize("dist/"),
		zippedPath			= path.normalize("zipped/"),
		contentFolder 		= "content",
		previewsPath		= path.normalize("src/previews/");

/**
*	Javascript libraries
*/
const libs	= [
				path.normalize('src/vendor/jquery/dist/jquery.min.js'),
				path.normalize('src/vendor/lodash/dist/lodash.min.js'),
				path.normalize('src/vendor/content-framework/dist/sb.all.min.js'),
			];
/**
 *	Helper functions
 */
function compileSass(vinyl) {
	var dest = vinyl.split('/');
	dest = dest[dest.indexOf('scss') - 1];
	return gulp.src(path.normalize(vinyl))
	.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass().on('error', sass.logError))
		.pipe(minifyCSS())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(path.join(distPath, dest, 'css')))
	.pipe(browserSync.reload({
		stream: true,
	}));
}
/**
 *	Self updates
 */
function selfUpdate(callback) {
	return run('cd .slush && git pull origin master && cd - ; cp .slush/core/.bowerrc . && cp .slush/core/bower.json . && cp .slush/core/package.json . && cp .slush/core/gulpfile.js .').exec(callback);
}
function updateLibs() {
	return run('bower update').exec();
}
gulp.task('update', function() {
	var originVersion = require('./.slush/core/package').version;
	var questions = {
			type: 'confirm',
			name:'updateLibs',
			message: 'Update vendor libraries:',
			default: false,
	};
	return inquirer.prompt(questions).then(function (answers) {
		if (answers.updateLibs) {
			updateLibs();
		}
		selfUpdate(function() {
			setTimeout(function() {
				run('npm install').exec(function() { setTimeout(function() { console.log('Successfully updated to version:', JSON.parse(fs.readFileSync('./.slush/core/package.json', 'utf8')).version); }, 1000); });
			}, 1000);
		});
	});
});
/**
*	Main tasks
*/
gulp.task('version', function() {
	var update = require('./.slush/version.json');
	console.log('Dev Tools version:', update.version);
});
gulp.task('clean', function() {
	if (distPath !== '.' && distPath !== path.normalize('./') && zippedPath !== '.' && zippedPath !== path.normalize('./'))
		del.sync([distPath, zippedPath]);
});
gulp.task('default', ['clean', 'serve'], function() {
	var contentWatcher = watch([
		path.join(piecescontentPath, path.normalize('**/*')),
		'!' + path.join(piecescontentPath, path.normalize('**/scss')),
		'!' + path.join(piecescontentPath, path.normalize('**/scss/*')),
		'!' + path.join(piecescontentPath, path.normalize('**/node_modules/*'))
	]).on('change', function(vinyl) {
		var changed = path.normalize(vinyl).split(piecescontentPath)[1];
		var dest = changed.substr(0, changed.lastIndexOf(path.normalize('/')));
		return gulp.src(path.normalize(vinyl))
			.pipe(gulp.dest(path.join(distPath, dest)));
	}).on('add', function(vinyl) {
		if (vinyl.indexOf('scss') === -1) {
			contentWatcher.add(vinyl);
			var changed = vinyl.split(piecescontentPath)[1];
			var distDest = distPath + changed.substr(0, changed.lastIndexOf(path.normalize('/')));
			return gulp.src(path.normalize(vinyl))
				.pipe(gulp.dest(distDest));
		}
	});
	var distWatcher = watch([path.join(distPath, path.normalize('**/*'))]).on('change', function(vinyl) {
		var dest = path.normalize(vinyl).split(distPath)[1];
		splitted = dest.split(path.normalize('/'));
		splitted[0] += '.zip';
		dest = '';
		splitted.forEach(function(val, index) { if (index < splitted.length - 1) dest+=val+path.normalize('/'); });
		return gulp.src(path.normalize(vinyl))
				.pipe(gulp.dest(path.join('.tmp', contentFolder, dest)));
	}).on('add', function(vinyl) {
		distWatcher.add(vinyl);
		var changed = vinyl.split(distPath)[1];
		var servDest = path.normalize('.tmp/'+contentFolder+'/') + changed.split(path.normalize('/'))[0] + '.zip'
			+ changed.split( changed.split(path.normalize('/'))[0] )[1];
		servDest = servDest.substr( 0, servDest.lastIndexOf(path.normalize('/')) );
		return gulp.src(path.normalize(vinyl))
			.pipe(gulp.dest(servDest));
	});
	var scssWatcher = watch([path.join(piecescontentPath, path.normalize('**/scss/*'))])
	.on('change', function(vinyl) {
		return compileSass(vinyl);
	}).on('add', function(vinyl) {
		scssWatcher.add(vinyl);
		return compileSass(vinyl);
	});
});
gulp.task('build', ['clean', 'zip'], function() {
	console.log('Production build complete.');
});

/**
* Common tasks
*/
gulp.task('compileSass', folders(piecescontentPath, function(folder) {
	return compileSass(path.join(piecescontentPath, folder, path.normalize('scss/app.scss')));
}));
gulp.task('rawFiles', function() {
	return gulp.src([
				path.join(piecescontentPath, '*.mp4'),
				path.join(piecescontentPath, '*.jpg'),
				path.join(piecescontentPath, '*.JPG'),
				path.join(piecescontentPath, '*.png'),
				path.join(piecescontentPath, '*.mp3'),
				path.join(piecescontentPath, '*.jpeg'),
				path.join(piecescontentPath, '*.JPEG'),
			])
			.pipe(gulp.dest(distPath))
			.pipe(gulp.dest(path.join('.tmp', contentFolder)));
});
gulp.task('buildContent', folders(piecescontentPath, function(folder) {
	return gulp.src([
				path.join(piecescontentPath, folder, path.normalize('**/*')),
				'!' + path.join(piecescontentPath, folder, path.normalize('scss/*')),
				'!' + path.join(piecescontentPath, folder, path.normalize('scss/')),
			])
			.pipe(gulp.dest(path.join(distPath, folder)));
}));

/**
* Dev specifics
*/
gulp.task('getFiles', ['rawFiles', 'compileSass', 'buildContent'], folders(distPath, function(folder) {
	return gulp.src(path.join(distPath, folder, path.normalize('**/*')))
			.pipe(gulp.dest(path.join('.tmp', contentFolder, folder + '.zip')));
}));
gulp.task('dev', ['getFiles'], function() {
	console.log('Dev build complete');
});

/**
* Prod specifics
*/
gulp.task('addPreviews', ['compileSass', 'buildContent', 'prodRawFiles'], folders(distPath, function(folder) {
	return gulp.src(path.join(previewsPath, folder)+'.png')
			.pipe(rename('preview.png'))
			.pipe(gulp.dest(path.join(distPath, folder)));
}));
gulp.task('prodRawFiles', ['rawFiles'], function() {
	return gulp.src([
				path.join(distPath, '*.mp4'),
				path.join(distPath, '*.jpg'),
				path.join(distPath, '*.png'),
				path.join(distPath, '*.mp3'),
				path.join(distPath, '*.jpeg'),
			])
			.pipe(gulp.dest(zippedPath));
});
gulp.task('zip', ['addPreviews'], folders(path.join(distPath), function(folder) {
	return gulp.src([
				path.join(distPath, folder, path.normalize('**/*')),
				'!' + path.join(distPath, '*.zip'),
				'!' + path.join(distPath, path.normalize('*.zip/*')),
			])
			.pipe(zip(folder + '.zip'))
			.pipe(gulp.dest(zippedPath));
}));

/**
* Slush
*/
/* @param patternArray [ { searchvalue: {string}, newvalue: {string} }, ... ] */
function slushGenFile(vinyl, dest, patternArray) {
	var fileName = vinyl.indexOf('index.html') !== -1 ? 'index.html' : vinyl;
	var tmp = fileName.split(path.normalize('/'));
	fileName = tmp.length > 0 ? tmp[tmp.length - 1] : fileName;
	var stream = gulp.src([vinyl]);
	if (patternArray) {
		if (patternArray.length) {
			for (var i = 0; i < patternArray.length; i++) {
				stream.pipe(replace(patternArray[i].searchvalue, patternArray[i].newvalue));
			}
		} else {
			stream.pipe(replace(patternArray.searchvalue, patternArray.newvalue));
		}
	}
	if (vinyl.indexOf('*') === -1)
		stream.pipe(rename(fileName));
	stream.pipe(gulp.dest(piecescontentPath+dest));
	return stream;
}
function slushGen(data) {
	data.orientation = data.orientation ? data.orientation : 'horizontal';
	slushGenFile(path.normalize('.slush/sets/'+data.type+'.index.html'),
		data.folderName,
		[
			{ searchvalue: '$(SLUCH_TITLE)', newvalue: data.folderName },
			{ searchvalue: '$(SLUCH_ORIENTATION)', newvalue: data.orientation === 'auto' ? '' : data.orientation },
		]);
	slushGenFile(path.normalize('.slush/sets/js/app.js'),
		path.normalize(data.folderName+'/js'),
		[
			{ searchvalue: '$(SLUSH_PROJECT_NAME)', newvalue: data.folderName },
			{ searchvalue: '$(SLUSH_AUTHOR)', newvalue: data.authorName },
			{ searchvalue: '$(SLUSH_EMAIL)', newvalue: data.authorEmail },
			{ searchvalue: '$(SLUSH_DATE)', newvalue: new Date() },
			{ searchvalue: '$(SLUCH_HQ)', newvalue: 'https://'+data.hq+'/' },
			{ searchvalue: '$(SLUCH_HQ)', newvalue: 'https://'+data.hq+'/' },
			{ searchvalue: '$(SLUCH_ORIENTATION)', newvalue: data.orientation === 'auto' ? 'horizontal' : data.orientation },

		]);
	slushGenFile(path.normalize('.slush/sets/js/helpers.js'), path.normalize(data.folderName+'/js'));
	slushGenFile(path.normalize('.slush/sets/scss/**/*.scss'), path.normalize(data.folderName+'/scss'));
	for (var i = 0; i < libs.length; i++) {
		slushGenFile(path.normalize(libs[i]), path.normalize(data.folderName+'/js'));
	}
}

function letsSlush() {
	var questions = [
		{
			type: 'string',
			name: 'folderName',
			message: 'content name:',
			validate: function(input) {
				if (input.length > 0) {
					try {
						var stats = fs.statSync(piecescontentPath+input);
						return '[Error] Provided name is already assigned to an existing Content Asset';
					} catch (err) {
						return true;
					}
				} else {
					return '[Error] name is required to add content to the project';
				}
			}
		},
		{
			type: 'string',
			name:'authorName',
			message: 'author:',
			default: process.platform === 'win32'? process.env.USERNAME : process.env.USER
		},
		{
			type: 'string',
			name:'authorEmail',
			message: 'author email:',
			validate: function(input) {
				console.log('validate');
				if (input.length === 0) {
					return true;
				} else {
					if (input.length > 5 && input.indexOf('@') > 0 && input.indexOf('.') > input.indexOf('@')) {
						return true;
					} else {
						return 'Please provide a valid email address or leave blank.';
					}
				}
			}
		},
		{
			type: 'list',
			name: 'type',
			message: 'content type:',
			choices: ['custom', 'rotator']
		},
		{
			type: 'list',
			name: 'orientation',
			message: 'orientation:',
			choices: ['horizontal', 'vertical', 'auto'],
			when: function(answers) {
				return answers.type === 'custom' ? true : false;
			}
		},
		{
			type: 'list',
			name: 'hq',
			message: 'hq:',
			choices: function() { return require('./.slush/json/hqs'); }
		},
	];
	return inquirer.prompt(questions).then(function (answers) {
		slushGen(answers);
		console.log('New Content Asset successfully created. Path: '+piecescontentPath+answers.folderName+'/');
	});
}
gulp.task('slush', function() {
	console.log('\nThis utility will walk you through generating a new Content Asset for your project.\nGenerated folder will be found in '+piecescontentPath+'\n\nDocumentation available at /doc/slush.html\n\nPress ^C at any time to quit.');
	return letsSlush();
});

/**
* Server specifics
*/
gulp.task('cleanServ', function() {
	return del.sync('.tmp');
});
gulp.task('serve', ['cleanServ', 'dev'], function() {
	return browserSync({
		files: [
			path.join('.tmp', contentFolder, path.normalize('**/*'))
		],
		server: {
			baseDir: '.tmp',
			directory: true,
		},
		startPath: 'content',
		port: 8080,
	});
});
