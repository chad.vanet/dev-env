// Configure Switchboard
(function() {
	'use strict';
	function findContentName() {
		return window.location.href.split('/').find(function(el) {
			return el.search('.zip') > 0;
		});
	}
	function isRawFile(fileName) {
		return (fileName.search('.jpg') > 0 || fileName.search('.jpeg') > 0 || fileName.search('.png') > 0) ? true : false;
	}

	const hq = SB.local ? 'https://parkassist.coatesdigital.com.au/' : window.location.protocol+'//'+window.location.host+'/';
	const datasourceName = 'Park-Assist-Rotator-Playlist.csv';
	const contentName = findContentName();

	var totalDuration = 0;
	var timeline;

	var playAnimation = {
		'fade-in': function(frame, data) { $(frame).css({ transition: 'opacity '+data['Transition In Seconds']+'s, transform '+data['Transition In Seconds']+'s' }); $(frame).css({ opacity: 1 }); },
		'horizontal-slide-top-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'horizontal-slide-right-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'horizontal-slide-btm-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'horizontal-slide-left-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'vertical-slide-top-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'vertical-slide-right-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'vertical-slide-btm-in': function(frame) { $(frame).css({ opacity: 1 }); },
		'vertical-slide-left-in': function(frame) { $(frame).css({ opacity: 1 }); },

		'fade-out': function(frame, data) { $(frame).css({ transition: 'none'}); $(frame).css({ opacity: 0 }); },
		'horizontal-slide-top-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'horizontal-slide-right-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'horizontal-slide-btm-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'horizontal-slide-left-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'vertical-slide-top-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'vertical-slide-right-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'vertical-slide-btm-out': function(frame) { $(frame).css({ opacity: 0 }); },
		'vertical-slide-left-out': function(frame) { $(frame).css({ opacity: 0 }); },
	};

	var activateFrame = function(frame) {
		if ($(frame)[0].contentWindow.activate) {
		 	$(frame)[0].contentWindow.activate();
		}
	};
	var prepareFrame = function(frame) {
		if ($(frame)[0].contentWindow.prepare) {
			$(frame)[0].contentWindow.prepare();
		}
	};
	var deactivateFrame = function(frame) {
		if ($(frame)[0].contentWindow.deactivate) {
		 	$(frame)[0].contentWindow.deactivate();
		}
		setTimeout(function() {
			$(frame).parent(".rotating-iframe").css({'z-index': 100});
			hideFrame(frame);
		}, 2000);
	};

	function hideFrame(frame) {
		$(frame).css({ display: 'none' });
	}

	function showFrame(frame) {
		$(frame).css({ display: 'block' });
	}

	function prepareNextFrame(frame) {
		prepareFrame(frame);
		showFrame(frame);
	}

	function displayNextFrame(data, frame) {

		var delay = 100;

		if (data['Transition Animation'] !== '(none)') {
			playAnimation[data['Transition Animation']+'-in']($(frame), data);
			delay = parseInt(data['Transition In Seconds']) * 1000 - 100;
		}
		window.setTimeout(function() {
			activateFrame($(frame));
		}, delay);

	}

	function initTimelineEvents(screens, el, index) {

		var TimeToTriggerNext = screens.map(m => m['Duration In Seconds'] ).reduce(function(total, currentDuration, currentIndex) {
			if (currentDuration.length === 0)
				currentDuration = 0;
			if (currentIndex <= index)
				return total + (parseInt(currentDuration) * 1000);
			return total;
		}, 0);

		var numOrder = parseInt(index);
		var currentID = index;
		var nextID = (numOrder < screens.length - 1 ? numOrder + 1 : 0).toString();

		timeline.addEvent(function() { // prepare next frame 3 seconds early
			if (window.activated) {
				if (screens[nextID]['Transition Animation'] !== '(none)') {
					playAnimation[screens[nextID]['Transition Animation']+'-out']($('#frame'+nextID+'>iframe'), screens[nextID]); // prepare come back animation
				}
				prepareNextFrame($('#frame'+nextID+'>iframe'));
				$('#frame'+currentID).css({'z-index': 9999}); // back to normal on top index
			}

		}, TimeToTriggerNext - 3000);

		timeline.addEvent(function() { // display next frame (50 milliseconds early)
			if (window.activated) {
				$('#frame'+nextID).css({'z-index': 10000}); // make sure always on top with higher index

				displayNextFrame(screens[nextID], $('#frame'+nextID+'>iframe')); // play Transition Animation + set active state

				deactivateFrame($('#frame'+currentID+'>iframe')); // deactivate previous current
			}
		}, TimeToTriggerNext - 50);
	}


	function deactivateAll() {
		var frames = Array.from(document.querySelectorAll('iframe'));
		frames.map(function(f) {
			if (f.contentWindow.deactivate)
				f.contentWindow.deactivate();
		});
	}

	window.activate = function() {
		window.activated = true;
		timeline.reset();
	};

	window.deactivate = function() {
		window.activated = false;
		deactivateAll();
	};

	function verifyDatasource(data) {
		if (data.length === 0) {
			console.error('Datasource error: Playlist is empty. [Datasource: '+datasourceName+'][Rotator: '+contentName+']');
			return null;
		}
		data.forEach(function(el, index) {
			if (el['Duration In Seconds'].length === 0) {
				el['Duration In Seconds'] = '0';
				console.warn('Datasource warning: Column "Duration In Seconds" empty. Default to', 0, '[Row:', index + 1, '][Rotator: '+obj.Rotator+']');
			}
		});
		data = data.filter(function(obj, index) {
			if (obj.Content.length === 0) {
				console.warn('Datasource warning: Column "Content" empty. [Row:', index + 1, '][Rotator: '+obj.Rotator+']');
			}
			if (obj['Duration In Seconds'] === '0') {
				console.warn('Datasource warning: Column "Duration In Seconds" =', 0, '. Cycle will be skipped. [Row:', index + 1, '][Rotator: '+obj.Rotator+']');
			}
			return (obj.Active === true && obj.Content.length > 0 && obj['Duration In Seconds'] != '0');
		});
		data.forEach(function(el, index) {
			if (el['Transition Animation'].length === 0) {
				el['Transition Animation'] = 'fade';
				console.warn('Datasource warning: Column "Transition Animation" empty. Default to "fade" [Row:', index + 1, '][Rotator: '+el.Rotator+']');
			}
			if (el['Transition In Seconds'].length === 0) {
				console.warn('Datasource warning: Column "Transition In Seconds" empty. Default to', 0, '[Row:', index + 1, '][Rotator: '+el.Rotator+']');
			} else if (parseInt(el['Transition In Seconds']) > 2) {
				console.warn('Datasource warning: Column "Transition In Seconds" =', parseInt(el['Transition In Seconds']),'. Maximum:', 2, '[Row:', index + 1, '][Rotator: '+el.Rotator+']');
			}
			el.Orientation = el.Orientation.toLowerCase();
			el['Transition Animation'] = el['Transition Animation'].toLowerCase();
		});
		data.sort(function(a, b) {
			return a['Rotation Order'] - b['Rotation Order'];
		});
		return data;
	}

	function start() {
		window.activated = true;
		var screens = SB.Dataset(SB.Data.get("rotator")).where('Rotator', '=', contentName);
		screens = verifyDatasource(screens);
		if (!screens) {
			return -1;
		}
		screens.forEach(function(el, index) {
			totalDuration += parseInt(el['Duration In Seconds']) * 1000;
		});

		timeline = new SB.Timeline({
			debug: false,
			framerate: 25,
			cycleTime: totalDuration
		});

		var iframeStructure = $('.rotating-iframe');
		if (screens[0].Orientation === 'horizontal') {
			$('html, body').css({width: '1920px', height: '1080px', overflow: 'hidden'});
		} else {
			$('html, body').css({width: '1080px', height: '1920px', overflow: 'hidden'});
		}
		$('#content').html('');

		// init iframes
		screens.forEach(function(el, index) {
			var newFrame = $(iframeStructure).clone();
			var src = '/content/' + el.Content;
			if (SB.local) {
				if (!isRawFile(el.Content))
					src += '/index.html';
			} else {
				src += '/' + window.location.search.replace('orientation=left', '');
			}
			$(newFrame).find('iframe').attr('src', src);
			$(newFrame).attr('id', 'frame'+index);

			$('#content').append($(newFrame));
			if (screens.length > 1) {
				initTimelineEvents(screens, el, index);
			}
		});
		if (screens.length === 1) {
			setTimeout(function() {
				activateFrame($('iframe'));
			}, 1000);
			setTimeout(function() { // back-up activation
				activateFrame($('iframe'));
			}, 5000);
		}
		deactivateAll();
		$('iframe').css({display: 'none'});
		$('#frame0').css({display: 'block', zIndex: 10000});
		activateFrame($('#frame0>iframe'));
	}

	SB.setup({
		url: hq,
		sources: [
			{ name: 'rotator', filename: datasourceName }
		],
		success: start
	});
})();