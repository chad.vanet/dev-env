describe("datasource class", function () {
  var DataSource = window._sb._dataSource;
  var instance = window._sb._dataSourceInstance;

  var oldJsonp = instance._jsonp;
  var mockJsonp = function (src, callback) {
    var data = JSON.stringify(config);
    if (src.indexOf('none'+yesterday) > 0)
      data = JSON.stringify(noneYesterday);
    else if (src.indexOf('none'+today) > 0)
      data = JSON.stringify(noneToday);
    else if (src.indexOf('none'+tomorrow) > 0)
      data = JSON.stringify(noneTomorrow);

    var fn = instance._toScriptTag(src, callback).src.split('=')[1];
    eval(fn + '(' + data + ')')
  };

  var noneToday = { myTodayObject: 'none' },
      noneYesterday = { myYesterdayObject: 'none' },
      noneTomorrow = { myTomorrowObject: 'none' };


  var today = new Date().toISOString().slice(0,19).replace(/-/g,'').replace(/:/g, '');
  var yesterday = new Date(Date.now() - 86400000).toISOString().slice(0,19).replace(/-/g,'').replace(/:/g, '');
  var tomorrow = new Date(Date.now() + 86400000).toISOString().slice(0,19).replace(/-/g,'').replace(/:/g, '');

  var config = {
    url: 'http://example.com/',
    sources: [
      'basic.csv',
      'advanced.csv',
      'something-' + tomorrow + '.csv',
      'everything-'+ today + '.csv',
      'none' + yesterday + '.csv',
      'none' + today + '.csv',
      'none' + tomorrow + '.csv'
    ]
  };

  var config2 = {
    base: 'http://example.com',
    sources: [
      { name:'not-basic', filename:'basic.csv' },
      'advanced.csv'
    ]
  };

  var config3 = {
    url: 'http://example.com/',
    sources: [
      'basic.csv',
      'advanced.csv',
      'something-' + tomorrow + '.csv',
      'everything-'+ today + '.csv',
      'none' + yesterday + '.csv',
      'none' + tomorrow + '.csv'
    ]
  };

  var config4 = {
    url: 'http://example.com/',
    sources: [
      { filename: 'basic.csv', name: 'basic' },
      { filename: 'advanced.csv', name: 'advanced' },
      'intermediate.csv'
    ]
  };

  beforeEach(function () {});
  afterEach(function () {});

  it('is singleton', function () {
    expect(new DataSource()).toBe(DataSource.instance());
  });

  it('can handle no datasources being attached', function () {
    expect(typeof instance.get()).toEqual('object');
  });

  it('can load a number of sources', function () {
    var scripts = document.querySelectorAll('script[data-switchboard]');
    expect(scripts.length).toBe(0);

    instance.load(config);
    scripts = document.querySelectorAll('script[data-switchboard]');
    expect(scripts.length).toBe(config.sources.length);
  });

  it('can get data', function () {
    // mock jsonp
    instance._jsonp = mockJsonp;
    instance.load(config);

    // assert
    expect(instance.get(config.sources[0])).toEqual(jasmine.objectContaining(config));
    expect(instance.get(config.sources[1])).toEqual(jasmine.objectContaining(config));
    expect(instance.get(config.sources[2])).toEqual(jasmine.objectContaining(config));
    expect(instance.get(config.sources[3])).toEqual(jasmine.objectContaining(config));

    instance._jsonp = oldJsonp;
  });

  it('can get all data', function () {
    instance._jsonp = mockJsonp;
    instance.load(config);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data === 'object').toBeTruthy();
    expect(Object.keys(data).length).toEqual(config.sources.length + methods);

    instance._jsonp = oldJsonp;
  });

  it('executes callback on data load', function () {
      var callback = jasmine.createSpy();

      // mock jsonp
      instance._jsonp = mockJsonp;
      config.success = callback;
      instance.load(config);

      // assert
      expect(callback.calls.count()).toBe(1);

      instance._jsonp = oldJsonp;
  });

  it('can generate api from url', function () {
    var urls = [
      {
        input:"http://example.com/",
        expected:"http://example.com/api/dataSource/"
      },
      {
        input:"http://example.com",
        expected:"http://example.com/api/dataSource/"
      },
      {
        input:"example.com/",
        expected:"example.com/api/dataSource/"
      }
    ];

    urls.map(function (url) {
      var actual = instance._toAPIFormat(url.input);
      expect(actual).toBe(url.expected);
    });
  });

  it('can generate a script element with callback', function () {
    var scripts = [
      { src:"https://example.com/?dummy=true", callback:"blah" },
      { src:"http://go.com", callback:"blue" },
      { src:"http://example.com", callback:"blot" },
      { src:"http://google.com", callback:"brap" }
    ];

    scripts.map(function (s) {
      var el = instance._toScriptTag(s.src, s.callback);
      expect(el.src).toContain(s.src);
      expect(el.src).toContain(s.callback);
      expect(el.src).not.toContain("0123456789");
      expect(el.dataset.switchboard).toBeTruthy();
    });
  });

  it('throws an error when trying to access non-existant data', function () {
    var getData = function () {
      instance.get('no-data');
    };
    expect(getData).toThrowError(/not exist/);
  });

  it('can check if data exists', function () {
    // mock jsonp
    instance._jsonp = mockJsonp;
    instance.load(config);

    // assert
    config.sources.map(function (s) {
      expect(instance.has(s)).toEqual(true);
    });

    expect(instance.has('no-data')).toBe(false);

    instance._jsonp = oldJsonp;
  });

  it('can get datasource using like', function () {
    instance._jsonp = mockJsonp;
    instance.load(config);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.like === 'function').toBeTruthy();
    expect(Object.keys(data.like('c.csv')).length).toEqual(1 + methods);
    expect(Object.keys(data.like('C.csv')).length).toEqual(1 + methods);
    expect(Object.keys(data.like('.csv')).length).toEqual(config.sources.length + methods);
    expect(Object.keys(data.like('thing-')).length).toEqual(2 + methods);

    instance._jsonp = oldJsonp;
  });

  it('can get the single datasource from a like query', function () {
    instance._jsonp = mockJsonp;
    instance.load(config);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.like === 'function').toBeTruthy();
    expect(data.like('c.csv').single().url).toEqual(config.url);
    expect(data.like('c.csv').single().sources).toEqual(config.sources);
    expect(data.like('c.csv').single()).toEqual(data.like('C.csv').single());

    instance._jsonp = oldJsonp;
  });

  it('can get appropriate scheduled datasource', function () {
    instance._jsonp = mockJsonp;
    instance.load(config);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.scheduled === 'function').toBeTruthy();
    expect(typeof data.scheduled()).toBe('object');
    expect(data.like('none').scheduled()).toEqual(jasmine.objectContaining(noneToday));

    instance._jsonp = oldJsonp;
  });

  it('doesnt get yesterdays datasource, today', function () {
    instance._jsonp = mockJsonp;
    instance.load(config);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.scheduled === 'function').toBeTruthy();
    expect(typeof data.scheduled()).toBe('object');
    expect(data.like('none').scheduled()).toEqual(jasmine.objectContaining(noneToday));

    instance._jsonp = oldJsonp;
  });

  it('gets yesterdays datasource if no today', function () {
    instance._jsonp = mockJsonp;
    instance.load(config3);

    var data = instance.get();
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.scheduled === 'function').toBeTruthy();
    expect(typeof data.scheduled()).toBe('object');
    expect(data.like('none').scheduled()).toEqual(jasmine.objectContaining(noneYesterday));

    instance._jsonp = oldJsonp;
  });

  it('gets the single datasource if no scheduled', function () {
    instance._jsonp = mockJsonp;
    instance.load(config3);

    var data = instance.get().like('c.csv');
    var methods = Object.keys(instance._addMethods({})).length;

    expect(typeof data.scheduled === 'function').toBeTruthy();
    expect(typeof data.scheduled()).toBe('object');
    expect(data.scheduled()).toEqual(data.single());

    instance._jsonp = oldJsonp;
  });

  it('gets a single datasource from all datasources', function () {
    instance._jsonp = mockJsonp;
    instance.load(config3);

    var data = instance.single();
    expect(data).not.toBe(null);

    instance._jsonp = oldJsonp;
  });

  it('single returns null if no datasources in query', function () {
    instance._jsonp = mockJsonp;
    instance.load(config3);

    var data = instance.get().like('nobody-knows-why-there-aint-no-data-sources');
    expect(data.single()).toEqual(null);

    instance._jsonp = oldJsonp;
  });

  it('gets like datasources using SB.Data.like()', function () {
      instance._jsonp = mockJsonp;
      instance.load(config);

      var methods = Object.keys(instance._addMethods({})).length;

      expect(typeof instance.like === 'function').toBeTruthy();
      expect(Object.keys(instance.like('c.csv')).length).toEqual(1 + methods);
      expect(Object.keys(instance.like('C.csv')).length).toEqual(1 + methods);
      expect(Object.keys(instance.like('.csv')).length).toEqual(config.sources.length + methods);
      expect(Object.keys(instance.like('thing-')).length).toEqual(2 + methods);

      instance._jsonp = oldJsonp;
  });

  describe('when fetching Data Sources', function () {
    beforeEach(function () {
      instance._jsonp = mockJsonp;
    });

    afterEach(function () {
      instance._jsonp = oldJsonp;
    });

    it('fetches a single Data Source', function (done) {
      var promise = instance.fetch(config.sources[0]);
      promise.then(function (data) {
        expect(data.length).not.toBeLessThan(0);
        expect(SB.Data.get(config.sources[0])).toBe(data);
        done();
      });
    });

    it('fetches a single Data Source with alias', function (done) {
      var promise = instance.fetch(config.sources[0], 'myFetchedDataSource');
      promise.then(function (data) {
        expect(data.length).not.toBeLessThan(0);
        expect(SB.Data.get('myFetchedDataSource')).toBe(data);
        done();
      });
    });

    it('reloads an existing Data Source', function (done) {
      spyOn(Q, 'when').and.callThrough();

      var fakeData = { notADataSource: 'hello '};
      SB.Data.set(config.sources[0], fakeData);

      var promise = instance.fetch(config.sources[0], null, true);
      promise.then(function (data) {
        expect(Q.when).not.toHaveBeenCalled();

        expect(data).toBe(SB.Data.get(config.sources[0]));
        expect(data).not.toBe(fakeData);

        done();
      });
    });

    it('doesnt reload an existing data source', function (done) {
      spyOn(Q, 'when').and.callThrough();

      var promise = instance.fetch(config.sources[0]);
      promise.then(function (data) {
        expect(Q.when).toHaveBeenCalled();
        done();
      });
    });

    it('fetches several Data Sources', function (done) {
      spyOn(Q, 'when').and.callThrough();

      var promise = instance.fetchAll([
        config.sources[0],
        config.sources[1]
      ]);

      promise.then(function (data) {
        expect(Q.when.calls.count()).toBe(2);
        expect(data.length).toBe(2);
        done();
      });
    });

    it('fetches several Data Sources with reloads', function () {
      spyOn(Q, 'when').and.callThrough();

      var promise = instance.fetchAll([
        config.sources[0],
        config.sources[1]
      ]);

      promise.then(function (data) {
        expect(Q.when.calls.count()).toBe(0);
        expect(data.length).toBe(2);
        done();
      });
    });

    describe('when getting a data source', function () {
      beforeEach(function () {
        // mock jsonp
        instance._jsonp = mockJsonp;
        instance.load(config4);
      });
      afterEach(function () {
        instance._jsonp = oldJsonp;
      });

      describe('when a filename and name are provided', function () {
        it('gets the data source with the name attached', function () {
          expect(SB.Data.get('basic').meta.name).toEqual('basic');
        });

        it('gets the data source with the filename attached', function () {
          expect(SB.Data.get('basic').meta.filename).toEqual('basic.csv');
        });
      });

      describe('when a filename and name are not provided', function () {
        it('gets the data source with the name attached', function () {
          expect(SB.Data.get('intermediate.csv').meta.name).toEqual('intermediate.csv');
        });

        it('gets the data source with the filename attached', function () {
          expect(SB.Data.get('intermediate.csv').meta.filename).toEqual('intermediate.csv');
        });
      });
    });
  });

});
