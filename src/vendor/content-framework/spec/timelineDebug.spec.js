describe("the timeline class", function () {

  var Timeline = window._sb._timeline;
  var timeline = null;

  beforeEach(function () {
    jasmine.clock().install();
    // set up timeline
    timeline = new Timeline({
      debug:true,
      framerate:10,
      cycleTime:5000
    });
  });

  afterEach(function () {
    jasmine.clock().uninstall();
    timeline.cleanUp();
  });

  it('adds canvas to the document', function () {
    expect(document.getElementById('sb-timeline-debug')).not.toBe(null);
  });

  

});
