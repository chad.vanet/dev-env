describe('dataset main class', function () {

  var Dataset = window._sb._dataset;
  var products, price, size, translate;

  products = [
    { id:1, name:"Hat" },
    { id:2, name:"Shoes" },
    { id:3, name:"Watch" }
  ];

  price = [
    { id:1, price:20 },
    { id:2, price:3 },
    { id:3, price:100 },
    { id:3, price:200 }
  ];

  size = [
    { id:1, size:["S","M","L"] },
    { id:2, size:[6, 8, 10, 12]},
    { id:3, size:[100, 200, 300, 400, 500]}
  ];

  translate = [
    { english:"Hat", hsilgne:"taH" },
    { english:"Shoes", hsilgne:"seohS" },
    { english:"Watch", hsilgne:"hctaW" },
  ];

  beforeEach(function () { });
  afterEach(function () {});

  it('exists', function () {
    expect(Dataset).not.toBeUndefined();
  });

  it('has methods that returns itself', function () {
    var dataset = Dataset(products);
    var dataset2 = dataset.join(price).on('id');
    expect(dataset).toBe(dataset2);

    dataset = Dataset(products);
    var dataset2 = dataset.join(price).on('id');
    expect(dataset).toBe(dataset2);
  });

  it('joins on single property', function () {

    var dataset = Dataset(products)
                    .join(price).on('id')
                    .get();

    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe("Hat");
    expect(p[1].price).toBe(20);

    expect(p[2].name).toBe("Shoes");
    expect(p[2].price).toBe(3);

    expect(p[3].name).toBe("Watch");
    expect(p[3].price).toBe(100);
  });

  it('joins on per dataset property', function () {
    var dataset = Dataset(products)
                    .join(translate)
                    .on('name', 'english').get();

    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe(p[1].english);
    expect(p[1].hsilgne).toBe("taH");

    expect(p[2].name).toBe(p[2].english);
    expect(p[2].hsilgne).toBe("seohS");

    expect(p[3].name).toBe(p[3].english);
    expect(p[3].hsilgne).toBe("hctaW");
  });

  it('joins many times using joinMany()', function () {
    var dataset = Dataset(products)
                    .joinMany(price, "prices").on('id')
                    .get();

    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe("Hat");
    expect(p[1].prices[0].price).toBe(20);

    expect(p[2].name).toBe("Shoes");
    expect(p[2].prices[0].price).toBe(3);

    expect(p[3].name).toBe("Watch");
    expect(p[3].prices.length).toBe(2);
    expect(p[3].prices[0].price).toBe(100);
    expect(p[3].prices[1].price).toBe(200);
  });

  it('can perform join using multiple sources', function () {
    var dataset = Dataset(products)
                    .join(price).on('id')
                    .join(size).on('id')
                    .join(translate).on('name', 'english')
                    .get();

    // get hats and confirm properties
    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe("Hat");
    expect(p[1].price).toBe(20);
    expect(p[1].name).toBe(p[1].english);
    expect(p[1].hsilgne).toBe("taH");

    expect(p[2].name).toBe("Shoes");
    expect(p[2].price).toBe(3);
    expect(p[2].name).toBe(p[2].english);
    expect(p[2].hsilgne).toBe("seohS");

    expect(p[3].name).toBe("Watch");
    expect(p[3].price).toBe(100);
    expect(p[3].name).toBe(p[3].english);
    expect(p[3].hsilgne).toBe("hctaW");
  });

  it ('can perform mix of join, join manys', function () {
    var dataset = Dataset(products)
                    .joinMany(price, 'prices').on('id')
                    .join(size).on('id')
                    .join(translate).on('name', 'english')
                    .get();

    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe("Hat");
    expect(p[1].prices.length).toBe(1);
    expect(p[1].name).toBe(p[1].english);
    expect(p[1].hsilgne).toBe("taH");

    expect(p[2].name).toBe("Shoes");
    expect(p[2].prices.length).toBe(1);
    expect(p[2].name).toBe(p[2].english);
    expect(p[2].hsilgne).toBe("seohS");

    expect(p[3].name).toBe("Watch");
    expect(p[3].prices.length).toBe(2);
    expect(p[3].name).toBe(p[3].english);
    expect(p[3].hsilgne).toBe("hctaW");
  });

  it ('can perform named join with single', function () {
    var dataset = Dataset(products)
                    .join(price, 'p').on('id')
                    .join(size, 's').on('id')
                    .get();

    var p = {};
    for (var i = 0; i < dataset.length; i++) {
      p[dataset[i].id] = dataset[i];
    }

    expect(p[1].name).toBe("Hat");
    expect(p[1].p.price).toBe(20);

    expect(p[2].name).toBe("Shoes");
    expect(p[2].p.price).toBe(3);

    expect(p[3].name).toBe("Watch");
    expect(p[3].p.price).toBe(100);
  });

  it('joinMany throws error if not provided name', function () {
    expect(function () {
      Dataset(products)
        .join(price).on('id')
        .joinMany(size).on('id')
        .get();
      }).toThrowError();
  });

  it('throws error if join called twice', function () {
    expect(function () {
      Dataset(products).join(price).join(size).on('id').get();
    }).toThrowError();
  });

  it('gets a collection of objects using where', function () {
    var dataset = Dataset(products)
                    .join(price).on('id')
                    .get();

    var filtered = Dataset(dataset).where('id', '=', 1);
    var filtered2 = Dataset(dataset).where('id', '>', 1);
    var filtered3 = Dataset(dataset).where('id', '<', 1);
    var filtered4 = Dataset(dataset).where('id', '!=', 1);

    expect(filtered.length).toEqual(1);
    expect(filtered2.length).toEqual(2);
    expect(filtered3.length).toEqual(0);
    expect(filtered4.length).toEqual(2);
  });

  it('throws error if invalid operator used', function () {
    expect(function () {
        Dataset(products)
            .join(price).on('id')
            .where('id', '==', 3);
    }).toThrowError();
  });
});
