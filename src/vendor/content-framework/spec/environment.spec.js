describe("environment class", function () {
  var Env = window._sb._environment;
  var instance = window._sb._environmentInstance;

  var oldJsonp = instance._jsonp;
  var mockJsonp = function (src, callback) {
    var data = JSON.stringify(example);
    var fn = instance._toScriptTag(src, callback).src;
    fn = fn.split('=');
    fn = fn[fn.length-1];
    eval(fn + '(' + data + ')');
  };

  var example = {
    "content-name": "basic-coffee.zip",
    "content-label": "Red",
    "content-modified": "2016-01-18T06:18:54.000Z",
    "content-groups": [ ],
    "channel-name": "",
    "screen-set-name": "",
    "sequence": null,
    "orientation": "normal"
  };

  var config = {
    url: 'http://example.com',
    environmentLocation: 'SOMETHING',
    content: 'basic-coffee.zip',
    success: function () {
      // do success
    }
  };

  beforeEach(function () {});
  afterEach(function () {});

  it('is singleton', function () {
    expect(new Env()).toBe(Env.instance());
  });

  it('can load environment data', function () {
    var scripts = document.querySelectorAll('script[data-switchboardenv]');
    expect(scripts.length).toBe(0);

    instance.load(config);
    scripts = document.querySelectorAll('script[data-switchboardenv]');
    expect(scripts.length).toBe(1);
  });

  it('can get data', function () {
    // mock jsonp
    instance._jsonp = mockJsonp;
    instance.load(config);

    // assert
    expect(instance.get()).toEqual(example);

    instance._jsonp = oldJsonp;
  });

  it('can mock data', function () {
    // mock jsonp
    instance._jsonp = mockJsonp;
    instance.load(config);

    // assert
    expect(instance.get()).toEqual(example);

    instance._jsonp = oldJsonp;
  });

  it('executes callback on data load', function () {
      var callback = jasmine.createSpy();

      // mock jsonp
      instance._jsonp = mockJsonp;
      config.success = callback;
      instance.load(config);

      // assert
      expect(callback.calls.count()).toBe(1);

      instance._jsonp = oldJsonp;
  });

  it('can generate api from url', function () {
    var urls = [
      {
        input:"http://example.com/",
        expected:"http://example.com/api/environmentData/"
      },
      {
        input:"http://example.com",
        expected:"http://example.com/api/environmentData/"
      },
      {
        input:"example.com/",
        expected:"example.com/api/environmentData/"
      }
    ];

    urls.map(function (url) {
      var actual = instance._toAPIFormat(url.input);
      expect(actual).toBe(url.expected);
    });
  });

  it('can generate a script element with callback', function () {
    var scripts = [
      { src:"https://example.com/?dummy=true" },
      { src:"http://go.com" },
      { src:"http://example.com" },
      { src:"http://google.com" }
    ];

    scripts.map(function (s) {
      var el = instance._toScriptTag(s.src);
      expect(el.src).toContain(s.src);
      expect(el.src).toContain('_complete');
      expect(el.src).not.toContain("0123456789");
      expect(el.dataset.switchboard).toBeTruthy();
    });
  });

  it('throws an error when trying to access non-existant data', function () {
    var getData = function () {
      instance.get('no-data');
    };
    expect(getData).toThrowError(/not exist/);
  });

  it('can check if data exists', function () {
    // mock jsonp
    instance._jsonp = mockJsonp;
    instance.load(config);

    // assert
    expect(instance.has("content-name")).toBe(true);
    expect(instance.has("content-label")).toBe(true);
    expect(instance.has("content-modified")).toBe(true);

    expect(instance.has('no-data')).toBe(false);

    instance._jsonp = oldJsonp;
  });


});
