describe("core class", function () {
  var QueryString = SB.QueryString; // window._sb._tool._queryString;
  var searchString = "?location=0&offset=1482901";

  it('gets null from an empty query string', function () {
    expect(QueryString.get('location')).toEqual(null);
  })

  it('gets the correct value from the query string', function () {
    expect(QueryString.get('location', searchString)).toEqual('0');
    expect(QueryString.get('offset', searchString)).toEqual('1482901');
  });

  it('returns null if value is not found', function () {
    expect(QueryString.get('not-location', searchString)).toEqual(null);
    expect(QueryString.get('not-offset', searchString)).toEqual(null);
    expect(QueryString.get('', searchString)).toEqual(null);
  });
});
