describe("core class", function () {

  beforeEach(function () {
    window._sb._reset();
  });

  it('resolves multiple requests correctly', function (done) {
    spyOn(SB.Data, 'load').and.callThrough();
    spyOn(SB.Env, 'load').and.callThrough();

    var promises = [
      SB.setup({
        url: 'http://example.com'
      }),
      SB.setup({
        url: 'http://example.com'
      })
    ];

    Q.all(promises).then(function () {
      expect(SB.Data.load.calls.count()).toBe(1);
      expect(SB.Env.load.calls.count()).toBe(1);
      done();
    });
  });

  it('resolves a promise when it sets up', function (done) {
    var promise = SB.setup({
      url: 'http://example.com'
    });

    promise.then(function () {
      expect(true).toBe(true);
      done();
    });
  });

  it('can setup without location or content', function () {
    var callback = jasmine.createSpy();

    SB.setup({
      url:'http://example.com',
      success: callback
    });

    expect(callback.calls.count()).toEqual(1);
  });

  it('has a dataset factory function', function () {
    expect(typeof(SB.Dataset)).toBe('function');
    expect(SB.Dataset([])._primary).toEqual([]);
  });

  it('has a data getter that gives the DataSource.instance()', function () {
    expect(typeof(SB.Data)).toBe('object');
  });

  it('adds core components to object', function () {
    var root = {};
    SB._exposeTo(root);

    expect(root._sb).toBeDefined();
    expect(root._sb._dataset).toBeDefined();
    expect(root._sb._timeline).toBeDefined();
    expect(root._sb._dataSource).toBeDefined();

    expect(root._sb._dataSourceInstance).toBeDefined();
  });

  it('can create Timeline objects', function () {
    expect(new SB.Timeline()).toBeTruthy();
  });
});
