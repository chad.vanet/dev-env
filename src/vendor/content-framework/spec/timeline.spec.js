describe("the timeline class", function () {

  var Timeline = window._sb._timeline;
  var timeline = null;

  beforeEach(function () {
    jasmine.clock().install();
    // set up timeline
    timeline = new Timeline({
      framerate:10,
      cycleTime:5000
    });
  });

  afterEach(function () {
    jasmine.clock().uninstall();
    timeline.cleanUp();
  });

  it('exists', function () {
    expect(typeof(Timeline) !== 'undefined').toBe(true);
  });

  it('adds a fixed event', function () {
      timeline.addEvent(function () {}, 0);

      expect(timeline._fixedEvents[0]).toBeDefined();
      expect(timeline._timelineEvents[0]).toBeDefined();
  });

  it('adds a interval event', function () {
    timeline.addEvent(function () {}, 0, 1000);

    expect(timeline._intervalEvents[0]).toBeDefined();

    for (var i = 0; i < 5000; i+=1000) {
      expect(timeline._timelineEvents[i]).toBeDefined();
    }
  });

  it('triggers the last event to the current time', function () {
    var callback = jasmine.createSpy("c"),
        callback2 = jasmine.createSpy("c2");

    timeline.addEvent(callback, 0);
    timeline.addEvent(callback2, 2500);

    timeline.triggerLatest(0);
    expect(callback.calls.count()).toBe(1);

    timeline.triggerLatest(2000);
    expect(callback.calls.count()).toBe(2);

    timeline.triggerLatest(2500);
    expect(callback.calls.count()).toBe(2);
    expect(callback2.calls.count()).toBe(1);

    timeline.triggerLatest(3000);
    expect(callback.calls.count()).toBe(2);
    expect(callback2.calls.count()).toBe(2);

    timeline.triggerLatest(-1);
    expect(callback.calls.count()).toBe(2);
    expect(callback2.calls.count()).toBe(2);
  })

  /*it('calls one fixed event per cycle', function () {
    var callback = jasmine.createSpy("callback");

    // mock date for use with jasmine.clock
    var currentDate = new Date(0);lp

    var date = Date;
    var timesCalled = 0;
    window.Date = function () {
      timesCalled++;
      return new date(currentDate.getTime() + timesCalled * 100);
    };

    // add fixed event
    timeline.addEvent(callback, 0);

    // two cycles
    jasmine.clock().tick(10000);

    expect(timeline._fixedEvents[0]).toBeDefined();
    expect(timeline._timelineEvents[0]).toBeDefined();
    expect(callback.calls.count()).toBe(2);
  });

  it('calls one interval events many times per cycle', function () {
    var callback = jasmine.createSpy("callback");

    // mock date for use with jasmine.clock
    var currentDate = new Date(0);
    var date = Date;
    var timesCalled = 0;
    window.Date = function () {
      timesCalled++;
      return new date(currentDate.getTime() + timesCalled * 100);
    };

    // add interval event
    timeline.addEvent(callback, 0, 100);

    // one cycles
    jasmine.clock().tick(500);

    expect(callback.calls.count()).toBe(500 / 100);
  });*/

  it('clears the timeline', function () {
    timeline.addEvent(function () {}, 0);
    timeline.addEvent(function () {}, 0);
    timeline.addEvent(function () {}, 0);

    expect(timeline._timelineEvents[0].length).toBe(3);

    timeline.clear();

    expect(timeline._timelineEvents[0]).toBeUndefined();
  });

  it('calculates nearest frame correctly', function () {
    expect(timeline._toFrameTime(2533)).toBe(2500);
    expect(timeline._toFrameTime(2490)).toBe(2400);
  });

  it('resets cyclePosition to 0', function () {
    timeline.reset();
    expect(timeline._cyclePosition).toEqual(0);
  });
});
