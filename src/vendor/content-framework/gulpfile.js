var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

function compile(watch) {
  var components = [
    { src: './src/SB.js', output: 'sb.js' }
  ];

  components.map(function (c) {
    var bundler = watchify(browserify(c.src, { debug: true }).transform(babel, {presets:["es2015"]}));

    function rebundle() {
      bundler.bundle()
        .on('error', function(err) { console.error(err); this.emit('end'); })
        .pipe(source(c.output))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build'));
    }

    if (watch) {
      bundler.on('update', function() {
        console.log('> bundling...');
        rebundle();
      });
    }

    rebundle();
  });
}

function compress() {
  return gulp.src([
                './lib/*.js',
                './build/sb.js'
              ])
              .pipe(concat('sb.all.js'))
              .pipe(gulp.dest('build'))
              .pipe(uglify({
                mangle: true
              }))
              .pipe(rename({
                extname: '.min.js'
              }))
              .pipe(gulp.dest('dist'));
}

function watch() {
  compile(true);
};

gulp.task('compress', function () { compress(); });
gulp.task('watch-compress', function () { return gulp.watch('./build/sb.js', ['compress']); });

gulp.task('build', function() { return compile(); });
gulp.task('watch-build', function() { return watch(); });

gulp.task('default', ['build', 'compress', 'watch-build', 'watch-compress']);
