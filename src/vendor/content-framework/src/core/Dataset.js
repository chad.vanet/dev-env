/**
 * Join arrays of related data
 * @example
 * var dataset = SB.Dataset(products)
 * 		.join(prices).on('id', 'product_id')
 * 		.get();
 */
class Dataset {

  /**
   * Initialises the dataset a single source of data.
   * @param  {object} dataset the primary datasource to combine to
   */
  constructor(dataset) {
    if (!Array.isArray(dataset))
      dataset = [dataset];

    this._primary = dataset;
    this._stack = [];
    this._callstack = [];
  }

  /**
   * Get all objects with prop 'eq' value
   * @param  {string} prop The property to get
   * @param  {string} op   The operator used to compare
   * @param  {any}    val  The value to compare
   * @return {Array}       Array of matching objects
   */
  where(prop, op, val) {
    var data = [];
    var ops = {
      '=' : function (x,y) { return x == y; },
      '!=': function (x,y) { return x != y; },
      '<=': function (x,y) { return x <= y; },
      '<' : function (x,y) { return x <  y; },
      '>=': function (x,y) { return x >= y; },
      '>' : function (x,y) { return x >  y; }
    };

    if (typeof ops[op] == 'undefined')
      throw new Error('Invalid operator (' + op + ')')

    this.get().map(function (m) {
      if (ops[op](m[prop], val))
        data.push(m);
    });

    return data;
  }

  /**
   * Schedule a one-to-one join to the primary dataset.
   * @param  {Array} dataset the related dataset
   * @param  {string} name    the property to attach the related values to (optional)
   * @return {Dataset}         this dataset object
   *
   * @example
   * var dataset = Dataset(products)
   * 		.join(prices).on('id', 'product_id')
   * 		.get();
   */
  join(dataset, name) {
    return this._join(dataset, name, "one");
  }

  /**
   * Schedule a one-to-many join to the primary dataset.
   * @param  {Array} dataset the related dataset
   * @param  {string} name    the property to attach the array of related values to (optional)
   * @return {Dataset}         this dataset object
   */
  joinMany(dataset, name) {
    if (arguments.length != 2)
      throw new Error("JoinMany requires a name to be provided");
    return this._join(dataset, name, "many");
  }

  /**
   * Adds a typed join to the stack.
   * @param  {Array} dataset the related dataset
   * @param  {string} name    the property to attach the related values to (optional)
   * @param  {string} type    the type of join being performed
   * @return {dataset}         this dataset object
   */
  _join(dataset, name, type) {
    if (this._callstack[this._callstack.length-1] != "join")
      this._stack.push({ name: name, dataset: dataset, key:null, foreignKey:null, type:type });
    else
      throw new Error("On must be called before performing another join.");

    // add call to stack
    this._callstack.push("join");

    return this;
  }

  /**
   * Add key descriptions to the most recent join, uses key as foreignKey if foreignKey is not provided.
   * @param  {string} key        the name of the matching property on the primary dataset
   * @param  {string} foreignKey the name of the matching property on the foreign dataset
   * @return {Dataset}            this dataset object
   */
  on(key, foreignKey) {
    if (arguments.length == 1)
      foreignKey = key;

    let latest = this._stack.pop();
    latest.key = key;
    latest.foreignKey = foreignKey;
    this._stack.push(latest);

    this._callstack.push("on");

    return this;
  }

  /**
   * Perform scheduled joins and return dataset as Array
   * @return {Array} The joined dataset
   */
  get() {
    // clear callstack
    this._callstack = [];

    // perform joins
    this._stack.reverse();
    while (this._stack.length > 0) {
      let join = this._stack.pop();

      this._primary.map(function (a) {
        // find the first matching value
        let b = [];
        for (let i = 0; i < join.dataset.length; i++) {
          let c = join.dataset[i];
          if (a[join.key] == c[join.foreignKey])
            b.push(c);
        }

        // copy over props
        if (join.type == "one") {
          b = b[0];

          if (join.name)
            a[join.name] = {};

          for (let prop in b) {
            if (join.name)
              a[join.name][prop] = b[prop];
            else
              a[prop] = b[prop];
          }
        }

        // set matching values to property
        if (join.type == "many") {
          a[join.name] = b;
        }
      });
    }

    return this._primary;
  }
}

/**
 * Assists with common Switchboard data operations
 */
export default Dataset;
