let TimelineDebugStyles = {
  canvas: {
    position: 'absolute',
    bottom:'0',
    left:'0',
    background:'#000',
    'border-top':'#f8f8f8 1px solid'
  }
};

let TDOuterPadding = 30;

let TDMargin = 2;
let TDBar = 4;

let TDOffsetY = 0.5;
let TDHeight = 0.3333333;

let TDColor = {
  unprogress: '#333',
  progress: '#666',

  eventFixed: 'rgba(255,178,102,1)',
  eventInterval: 'rgba(102,204,255,1)',

  eventFixedDone: 'rgba(225,148,72,1)',
  eventIntervalDone: 'rgba(72,174,225,1)',

  eventTag: '#fff',

  text: '#FFFFFF'
};

/**
 * Debug class for timeline
 */
class TimelineDebug {

  /**
   * Set up TimelineDebug class
   * @param  {Boolean} debug Whether or not to draw the debug
   */
  constructor(debug = false) {
    this._debug = debug;

    this._frame = 0;
    this._cycle = 0;
    this._events = [];

    this._mouse = { x: 0, y: 0 };
    this._tag = "";

    if (this._debug) {
      this._canvas = document.createElement('canvas');
      this._canvas.id = "sb-timeline-debug";

      for (var key in TimelineDebugStyles.canvas)
        this._canvas.style[key] = TimelineDebugStyles.canvas[key];
      this._canvas.style.bottom = ((this._debug - 1) * 10) + '%';

      this._ctx = this._canvas.getContext('2d');
      document.body.appendChild(this._canvas);

      window.addEventListener('resize', () => {
        this.update(this._frame, this._cycle, this._events);
      });
      this._resizeCanvas();

      this._ctx.font = '20px Arial';
      this._ctx.textAlign = 'center';
      this._ctx.fillStyle = TDColor.text;
      this._ctx.fillText("Timeline Inactive", this._canvas.width / 2, this._canvas.height / 2);

      window.addEventListener('mousemove', (e) => {
        this._setMouse(e);
      });
    }
  }

  /**
   * Update the timeline debug with timeline data and draw
   * @param  {Number} frame  The current time within the timeline
   * @param  {Number} cycle  The length of the timeline cycle
   * @param  {Array} events Array of events to draw
   */
  update(frame, cycle, events) {
    this._frame = frame;
    this._cycle = cycle;
    this._events = events;

    if (this._debug) {
      let canvas = this._canvas;
      let ctx = this._ctx;
      this._resizeCanvas();

      //
      let width = canvas.width - TDOuterPadding * 2;

      // clear draw
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      let margin = TDMargin;
      let bar = TDBar;

      // draw progress
      let progress = 0;
      if (cycle)
        progress = Math.floor(frame / cycle * width / (margin + bar)) * (margin + bar);

      for (let i = 0; i < width; i += (margin + bar)) {
        ctx.fillStyle = TDColor.unprogress;
        if (i < progress)
          ctx.fillStyle = TDColor.progress;

        ctx.fillRect(i+TDOuterPadding, canvas.height * TDOffsetY, bar, canvas.height * TDHeight);
      }

      // draw events
      this._eventTag = "";
      for (let key in events) {
        for (let i = 0; i < events[key].length; i++) {
          let event = events[key][i];
          this._drawEvent(progress, cycle, key, event, events[key]);
        }
      }
      this._drawEventTag(this._eventTag, TDOuterPadding, TDOuterPadding);

      ctx.fillStyle = "#000";
      ctx.fillRect(0, canvas.height * (TDOffsetY + TDHeight * 0.5), canvas.width, 1);
    }
  }

  /**
   * Draw event to the timeline
   * @param  {Number} progress Current position in timeline
   * @param  {Number} cycle    Total length of timeline
   * @param  {Number} time     Event position in timeline
   * @param  {TimelineEvent} event    Event to draw
   * @param  {TimelineEvents[]} events   All the events for the current position
   */
  _drawEvent(progress, cycle, time, event, events) {
    let canvas = this._canvas;
    let ctx = this._ctx;

    let width = canvas.width - TDOuterPadding * 2;

    let margin = TDMargin;
    let bar = TDBar;

    let isFixed = this._isFixed(event);
    let color = TDColor.eventFixed,
        doneColor = TDColor.eventFixedDone,
        offsetY = canvas.height * TDHeight * 0.5,
        offsetH = offsetY;

    if (!isFixed) {
      color = TDColor.eventInterval;
      doneColor = TDColor.eventIntervalDone;
      offsetY = 0;
      offsetH = canvas.height * TDHeight * 0.5;
    }

    let pos = {
      x: Math.floor(time / cycle * width / (margin + bar)) * (margin + bar) + TDOuterPadding,
      y: canvas.height * TDOffsetY + offsetY
    };

    ctx.fillStyle = color;
    if (pos < progress)
      ctx.fillStyle = doneColor;

    ctx.fillRect(
      pos.x, pos.y,
      bar, offsetH
    );

    // Add event name to label
    let tagOffset = isFixed ? offsetH + 12 : -10;
    if (this._isOver(pos.x, pos.y, bar, offsetH))
      this._addEventTag((event.tag || '[No Tag]') + ' ' + (time / 1000).toFixed(3) + 's');
  }

  /**
   * Add current event tag to current event label
   * @param {string} tag The tag describing the event
   */
  _addEventTag(tag) {
    if (this._eventTag != "")
      this._eventTag += ", ";
    this._eventTag += tag;
  }

  /**
   * Draw the event tag to canvas
   * @param  {string} tag The tag to draw
   * @param  {Number} x   The x coord to draw text
   * @param  {Number} y   The y coord to draw text
   */
  _drawEventTag(tag, x, y) {
    this._ctx.textAlign = 'left';
    this._ctx.font = '20px Arial';
    this._ctx.fillStyle = TDColor.eventTag;
    this._ctx.fillText(tag, x, y);
  }

  /**
   * Resize the canvas on window resize event
   */
  _resizeCanvas() {
    this._canvas.width = window.innerWidth;
    this._canvas.height = window.innerHeight * 0.1;
  }

  /**
   * Is an event a fixed event
   * @param  {TimelineEvent}  event The event to determine is fixed
   * @return {Boolean}       Event is instance of FixedTimelineEvent
   */
  _isFixed(event) {
    return (typeof(event.interval) === 'undefined');
  }

  /**
   * Check if mouse is over certain area
   * @param  {Number}  x Top x coord of area
   * @param  {Number}  y Top y coord of area
   * @param  {Number}  w Width of the area
   * @param  {Number}  h Height of the area
   * @return {Boolean}   Is the mouse over the area specified
   */
  _isOver(x, y, w, h) {
    // check if mouse is over box
    let m = this._mouse;

    let padding = 4;
    x -= padding;
    y -= padding;
    w += padding * 2;
    h += padding * 2;

    return (
      x < m.x &&
      x + w > m.x &&
      y < m.y &&
      y + h > m.y
    );
  }

  /**
   * Handle mouse move event
   * @param {Event} e The mouse move event object
   */
  _setMouse(e) {
    let rect = this._canvas.getBoundingClientRect();
    this._mouse = {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  }
};

export default TimelineDebug;
