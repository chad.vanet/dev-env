import {Array} from './core/Polyfill';
import Core from './core/Core';

(function () {

  /**
   * Expose core components for use in testing / jsonp
   * @param  {[type]} window [description]
   * @return {[type]}        [description]
   */
  Core._exposeTo(window);

  /**
   * The main switchboard object containing
   * all switchboard-client functionality
   */
  window.SB = Core;

})();
