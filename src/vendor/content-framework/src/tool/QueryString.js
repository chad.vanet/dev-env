/**
 * Wraps the core components
 */
class QueryString {

  /**
   * Get a query string parameter
   * @param  {string} param The name of the value to get
   * @return {string}       The corresponding value
   */
  static get(param, searchString = window.location.search) {
    let search = searchString.substring(1);
    let vars = search.split('&');

    for (let i = 0; i < vars.length; i++) {
      let keyvalue = vars[i].split('=');
      if (keyvalue[0] == param)
        return keyvalue[1];
    }

    return null;
  }
};

/**
 * Provide helper function for dealing with url
 */
export default QueryString;
